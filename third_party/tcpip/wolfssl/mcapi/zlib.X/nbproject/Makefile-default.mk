#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/zlib.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/zlib.X.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../../zlib-1.2.8/adler32.c ../../zlib-1.2.8/compress.c ../../zlib-1.2.8/crc32.c ../../zlib-1.2.8/deflate.c ../../zlib-1.2.8/gzclose.c ../../zlib-1.2.8/gzlib.c ../../zlib-1.2.8/gzread.c ../../zlib-1.2.8/gzwrite.c ../../zlib-1.2.8/infback.c ../../zlib-1.2.8/inffast.c ../../zlib-1.2.8/inflate.c ../../zlib-1.2.8/inftrees.c ../../zlib-1.2.8/trees.c ../../zlib-1.2.8/uncompr.c ../../zlib-1.2.8/zutil.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/608321699/adler32.o ${OBJECTDIR}/_ext/608321699/compress.o ${OBJECTDIR}/_ext/608321699/crc32.o ${OBJECTDIR}/_ext/608321699/deflate.o ${OBJECTDIR}/_ext/608321699/gzclose.o ${OBJECTDIR}/_ext/608321699/gzlib.o ${OBJECTDIR}/_ext/608321699/gzread.o ${OBJECTDIR}/_ext/608321699/gzwrite.o ${OBJECTDIR}/_ext/608321699/infback.o ${OBJECTDIR}/_ext/608321699/inffast.o ${OBJECTDIR}/_ext/608321699/inflate.o ${OBJECTDIR}/_ext/608321699/inftrees.o ${OBJECTDIR}/_ext/608321699/trees.o ${OBJECTDIR}/_ext/608321699/uncompr.o ${OBJECTDIR}/_ext/608321699/zutil.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/608321699/adler32.o.d ${OBJECTDIR}/_ext/608321699/compress.o.d ${OBJECTDIR}/_ext/608321699/crc32.o.d ${OBJECTDIR}/_ext/608321699/deflate.o.d ${OBJECTDIR}/_ext/608321699/gzclose.o.d ${OBJECTDIR}/_ext/608321699/gzlib.o.d ${OBJECTDIR}/_ext/608321699/gzread.o.d ${OBJECTDIR}/_ext/608321699/gzwrite.o.d ${OBJECTDIR}/_ext/608321699/infback.o.d ${OBJECTDIR}/_ext/608321699/inffast.o.d ${OBJECTDIR}/_ext/608321699/inflate.o.d ${OBJECTDIR}/_ext/608321699/inftrees.o.d ${OBJECTDIR}/_ext/608321699/trees.o.d ${OBJECTDIR}/_ext/608321699/uncompr.o.d ${OBJECTDIR}/_ext/608321699/zutil.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/608321699/adler32.o ${OBJECTDIR}/_ext/608321699/compress.o ${OBJECTDIR}/_ext/608321699/crc32.o ${OBJECTDIR}/_ext/608321699/deflate.o ${OBJECTDIR}/_ext/608321699/gzclose.o ${OBJECTDIR}/_ext/608321699/gzlib.o ${OBJECTDIR}/_ext/608321699/gzread.o ${OBJECTDIR}/_ext/608321699/gzwrite.o ${OBJECTDIR}/_ext/608321699/infback.o ${OBJECTDIR}/_ext/608321699/inffast.o ${OBJECTDIR}/_ext/608321699/inflate.o ${OBJECTDIR}/_ext/608321699/inftrees.o ${OBJECTDIR}/_ext/608321699/trees.o ${OBJECTDIR}/_ext/608321699/uncompr.o ${OBJECTDIR}/_ext/608321699/zutil.o

# Source Files
SOURCEFILES=../../zlib-1.2.8/adler32.c ../../zlib-1.2.8/compress.c ../../zlib-1.2.8/crc32.c ../../zlib-1.2.8/deflate.c ../../zlib-1.2.8/gzclose.c ../../zlib-1.2.8/gzlib.c ../../zlib-1.2.8/gzread.c ../../zlib-1.2.8/gzwrite.c ../../zlib-1.2.8/infback.c ../../zlib-1.2.8/inffast.c ../../zlib-1.2.8/inflate.c ../../zlib-1.2.8/inftrees.c ../../zlib-1.2.8/trees.c ../../zlib-1.2.8/uncompr.c ../../zlib-1.2.8/zutil.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/zlib.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX795F512L
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/608321699/adler32.o: ../../zlib-1.2.8/adler32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/adler32.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/adler32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/adler32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/adler32.o.d" -o ${OBJECTDIR}/_ext/608321699/adler32.o ../../zlib-1.2.8/adler32.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/compress.o: ../../zlib-1.2.8/compress.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/compress.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/compress.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/compress.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/compress.o.d" -o ${OBJECTDIR}/_ext/608321699/compress.o ../../zlib-1.2.8/compress.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/crc32.o: ../../zlib-1.2.8/crc32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/crc32.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/crc32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/crc32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/crc32.o.d" -o ${OBJECTDIR}/_ext/608321699/crc32.o ../../zlib-1.2.8/crc32.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/deflate.o: ../../zlib-1.2.8/deflate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/deflate.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/deflate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/deflate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/deflate.o.d" -o ${OBJECTDIR}/_ext/608321699/deflate.o ../../zlib-1.2.8/deflate.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/gzclose.o: ../../zlib-1.2.8/gzclose.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzclose.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzclose.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/gzclose.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/gzclose.o.d" -o ${OBJECTDIR}/_ext/608321699/gzclose.o ../../zlib-1.2.8/gzclose.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/gzlib.o: ../../zlib-1.2.8/gzlib.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzlib.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzlib.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/gzlib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/gzlib.o.d" -o ${OBJECTDIR}/_ext/608321699/gzlib.o ../../zlib-1.2.8/gzlib.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/gzread.o: ../../zlib-1.2.8/gzread.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzread.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzread.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/gzread.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/gzread.o.d" -o ${OBJECTDIR}/_ext/608321699/gzread.o ../../zlib-1.2.8/gzread.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/gzwrite.o: ../../zlib-1.2.8/gzwrite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzwrite.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzwrite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/gzwrite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/gzwrite.o.d" -o ${OBJECTDIR}/_ext/608321699/gzwrite.o ../../zlib-1.2.8/gzwrite.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/infback.o: ../../zlib-1.2.8/infback.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/infback.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/infback.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/infback.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/infback.o.d" -o ${OBJECTDIR}/_ext/608321699/infback.o ../../zlib-1.2.8/infback.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/inffast.o: ../../zlib-1.2.8/inffast.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/inffast.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/inffast.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/inffast.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/inffast.o.d" -o ${OBJECTDIR}/_ext/608321699/inffast.o ../../zlib-1.2.8/inffast.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/inflate.o: ../../zlib-1.2.8/inflate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/inflate.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/inflate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/inflate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/inflate.o.d" -o ${OBJECTDIR}/_ext/608321699/inflate.o ../../zlib-1.2.8/inflate.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/inftrees.o: ../../zlib-1.2.8/inftrees.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/inftrees.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/inftrees.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/inftrees.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/inftrees.o.d" -o ${OBJECTDIR}/_ext/608321699/inftrees.o ../../zlib-1.2.8/inftrees.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/trees.o: ../../zlib-1.2.8/trees.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/trees.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/trees.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/trees.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/trees.o.d" -o ${OBJECTDIR}/_ext/608321699/trees.o ../../zlib-1.2.8/trees.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/uncompr.o: ../../zlib-1.2.8/uncompr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/uncompr.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/uncompr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/uncompr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/uncompr.o.d" -o ${OBJECTDIR}/_ext/608321699/uncompr.o ../../zlib-1.2.8/uncompr.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/zutil.o: ../../zlib-1.2.8/zutil.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/zutil.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/zutil.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/zutil.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/zutil.o.d" -o ${OBJECTDIR}/_ext/608321699/zutil.o ../../zlib-1.2.8/zutil.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
else
${OBJECTDIR}/_ext/608321699/adler32.o: ../../zlib-1.2.8/adler32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/adler32.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/adler32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/adler32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/adler32.o.d" -o ${OBJECTDIR}/_ext/608321699/adler32.o ../../zlib-1.2.8/adler32.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/compress.o: ../../zlib-1.2.8/compress.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/compress.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/compress.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/compress.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/compress.o.d" -o ${OBJECTDIR}/_ext/608321699/compress.o ../../zlib-1.2.8/compress.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/crc32.o: ../../zlib-1.2.8/crc32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/crc32.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/crc32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/crc32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/crc32.o.d" -o ${OBJECTDIR}/_ext/608321699/crc32.o ../../zlib-1.2.8/crc32.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/deflate.o: ../../zlib-1.2.8/deflate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/deflate.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/deflate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/deflate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/deflate.o.d" -o ${OBJECTDIR}/_ext/608321699/deflate.o ../../zlib-1.2.8/deflate.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/gzclose.o: ../../zlib-1.2.8/gzclose.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzclose.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzclose.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/gzclose.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/gzclose.o.d" -o ${OBJECTDIR}/_ext/608321699/gzclose.o ../../zlib-1.2.8/gzclose.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/gzlib.o: ../../zlib-1.2.8/gzlib.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzlib.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzlib.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/gzlib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/gzlib.o.d" -o ${OBJECTDIR}/_ext/608321699/gzlib.o ../../zlib-1.2.8/gzlib.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/gzread.o: ../../zlib-1.2.8/gzread.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzread.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzread.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/gzread.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/gzread.o.d" -o ${OBJECTDIR}/_ext/608321699/gzread.o ../../zlib-1.2.8/gzread.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/gzwrite.o: ../../zlib-1.2.8/gzwrite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzwrite.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/gzwrite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/gzwrite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/gzwrite.o.d" -o ${OBJECTDIR}/_ext/608321699/gzwrite.o ../../zlib-1.2.8/gzwrite.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/infback.o: ../../zlib-1.2.8/infback.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/infback.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/infback.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/infback.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/infback.o.d" -o ${OBJECTDIR}/_ext/608321699/infback.o ../../zlib-1.2.8/infback.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/inffast.o: ../../zlib-1.2.8/inffast.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/inffast.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/inffast.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/inffast.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/inffast.o.d" -o ${OBJECTDIR}/_ext/608321699/inffast.o ../../zlib-1.2.8/inffast.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/inflate.o: ../../zlib-1.2.8/inflate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/inflate.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/inflate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/inflate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/inflate.o.d" -o ${OBJECTDIR}/_ext/608321699/inflate.o ../../zlib-1.2.8/inflate.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/inftrees.o: ../../zlib-1.2.8/inftrees.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/inftrees.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/inftrees.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/inftrees.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/inftrees.o.d" -o ${OBJECTDIR}/_ext/608321699/inftrees.o ../../zlib-1.2.8/inftrees.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/trees.o: ../../zlib-1.2.8/trees.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/trees.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/trees.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/trees.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/trees.o.d" -o ${OBJECTDIR}/_ext/608321699/trees.o ../../zlib-1.2.8/trees.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/uncompr.o: ../../zlib-1.2.8/uncompr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/uncompr.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/uncompr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/uncompr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/uncompr.o.d" -o ${OBJECTDIR}/_ext/608321699/uncompr.o ../../zlib-1.2.8/uncompr.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
${OBJECTDIR}/_ext/608321699/zutil.o: ../../zlib-1.2.8/zutil.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/608321699" 
	@${RM} ${OBJECTDIR}/_ext/608321699/zutil.o.d 
	@${RM} ${OBJECTDIR}/_ext/608321699/zutil.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/608321699/zutil.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -Os -DHAVE_HIDDEN -DMAX_MEM_LEVEL=1 -DMAX_WBITS=11 -I"../../zlib-1.2.8" -MMD -MF "${OBJECTDIR}/_ext/608321699/zutil.o.d" -o ${OBJECTDIR}/_ext/608321699/zutil.o ../../zlib-1.2.8/zutil.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp=${DFP_DIR}
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: archive
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/zlib.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  r dist/${CND_CONF}/${IMAGE_TYPE}/zlib.X.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    
else
dist/${CND_CONF}/${IMAGE_TYPE}/zlib.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  r dist/${CND_CONF}/${IMAGE_TYPE}/zlib.X.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default
